package com.sfauto.senflotteautomobiles.services;

import java.util.*;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.sfauto.senflotteautomobiles.Model.Client;
import com.sfauto.senflotteautomobiles.dao.ClientRepository;

@Service
@Transactional
public class ClientService {
	
	private ClientRepository clientrepository;

	@Autowired
	public ClientService(ClientRepository clientrepository) {
		
		this.clientrepository = clientrepository;
	}
	
	//liste des clients
	
	public List<Client> getAllClient(){
		
		return clientrepository.findAll();
	}
	

	//add
		public long addClient(Client client ) {
			
			Client m = new Client();
			
			m.setNom(client.getNom());
			m.setPrenom(client.getPrenom());
			m.setAddress(client.getAddress());
			m.setMail(client.getMail());
			m.setNumtel(client.getNumtel());
			m.setPassword(client.getPassword());
			m.setPhoto(client.getPhoto());
			
			m.setRole("CLIENT");
			
			return clientrepository.save(m).getIduser();
			
		}
	
	
	//login
	public Optional<Client> loginclient(String mail) {
		
		return clientrepository.findClientByMail(mail);
	}
	
	//totalclient
	public int totalClient() {
		return clientrepository.nombreClient();
	}
	
	//findbyid
	public Optional<Client> fiendClientid(long id){
		return clientrepository.findClientByIduser(id);
	}
	

}
