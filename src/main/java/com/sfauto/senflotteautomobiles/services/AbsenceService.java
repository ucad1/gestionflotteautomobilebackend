package com.sfauto.senflotteautomobiles.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.sfauto.senflotteautomobiles.Model.Absence;
import com.sfauto.senflotteautomobiles.dao.AbsenceRepository;
import com.sfauto.senflotteautomobiles.dao.ChauffeurRepository;
import com.sfauto.senflotteautomobiles.dao.MecanicienRepository;

@Service
@Transactional
public class AbsenceService {

	private AbsenceRepository absencerepo;
	private MecanicienRepository mecanorepo;
	private ChauffeurRepository chrepo;
	
	@Autowired
	public AbsenceService(AbsenceRepository absencerepo,MecanicienRepository mecanorepo, ChauffeurRepository chrepo) {
		
		this.absencerepo = absencerepo;
		this.mecanorepo = mecanorepo;
		this.chrepo = chrepo;
	}
	
	public  ResponseEntity<Absence> absenceMecano(long id, Absence abs) {
		
		Absence ab = mecanorepo.findMecanicienByIduser(id).map(mec -> {
			abs.setMecanicien(mec);
			
			return absencerepo.save(abs);
		}).orElseThrow();
		
		return new ResponseEntity<>(ab, HttpStatus.CREATED);
		
	}
	
	//liste absence
	public List<Absence> getAllAbsence(){
		return absencerepo.listabsence();
	}
	
	/***

	public Optional<Absence> getAbsenceMecano(long id){
		return absencerepo. findAbsenceByMecanicienid(id);
	} **/
}
