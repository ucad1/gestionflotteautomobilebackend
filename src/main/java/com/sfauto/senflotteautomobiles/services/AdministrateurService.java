package com.sfauto.senflotteautomobiles.services;

import java.util.*;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfauto.senflotteautomobiles.Model.Administrateur;
import com.sfauto.senflotteautomobiles.dao.AdministrateurRepository;

@Service
@Transactional
public class AdministrateurService {
	
	
	private AdministrateurRepository adminrepo;

	@Autowired
	public AdministrateurService(AdministrateurRepository adminrepo) {
	
		this.adminrepo = adminrepo;
	}
	
	//liste admin
	
	public List<Administrateur> getAlladmin(){
		
		return adminrepo.findAll();
	}
	

	//add
		public long addAdmin(Administrateur admin ) {
			
			Administrateur m = new Administrateur();
			
			m.setNom(admin.getNom());
			m.setPrenom(admin.getPrenom());
			m.setAddress(admin.getAddress());
			m.setMail(admin.getMail());
			m.setNumtel(admin.getNumtel());
			m.setPassword(admin.getPassword());
			m.setPhoto(admin.getPhoto());
			
			m.setRole("ADMIN");
			
			return adminrepo.save(m).getIduser();
			
		}
	
	//login
	public Optional<Administrateur> loginadmin(String mail){
		
		return adminrepo.findAdministrateurByMail(mail);
	}
	
	//
	public Optional<Administrateur> findAdminId(long id){
		return adminrepo.findAdministrateurByIduser(id);
	}
	
	

}
