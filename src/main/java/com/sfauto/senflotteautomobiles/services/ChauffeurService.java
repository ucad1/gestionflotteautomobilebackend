package com.sfauto.senflotteautomobiles.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfauto.senflotteautomobiles.Model.Chauffeur;
import com.sfauto.senflotteautomobiles.dao.ChauffeurRepository;
import com.sfauto.senflotteautomobiles.dao.MecanicienRepository;

@Service
@Transactional
public class ChauffeurService {

	private ChauffeurRepository chauffeurrepository;
	
	private MecanicienRepository mrepo;

	@Autowired
	public ChauffeurService(ChauffeurRepository chauffeurrepository,MecanicienRepository mrepo) {
		
		this.chauffeurrepository = chauffeurrepository;
		this.mrepo = mrepo;
	}
	
	//liste des chauffeurs
	public List<Chauffeur> getAllChauffeur(){
		
		System.out.println("Get all chauffeurs");
		return chauffeurrepository.findAll();
	}
	
	
	//ajout chauffeur
	public long ajoutchauffeur(Chauffeur chauffeur) {
		
		Chauffeur ch =new Chauffeur();
		
		ch.setNom(chauffeur.getNom());
		ch.setPrenom(chauffeur.getPrenom());
		ch.setMail(chauffeur.getMail());
		ch.setNumeropermis(chauffeur.getNumeropermis());
		ch.setTypepermis(chauffeur.getTypepermis());
		ch.setDatedelivrance(chauffeur.getDatedelivrance());
		ch.setDateexpiration(chauffeur.getDateexpiration());
		ch.setNumtel(chauffeur.getNumtel());
		ch.setPassword(chauffeur.getPassword());
		ch.setAddress(chauffeur.getAddress());
		ch.setRole("CHAUFFEUR");
		
		return chauffeurrepository.save(ch).getIduser();
	}
	
	//nombre chauffeur
	public int totalChauff() {
		
		int nombremecano = mrepo.nombreMecano();
		int nombrechff =  chauffeurrepository.nombreChauffeur();
		
		return nombremecano + nombrechff;
	}
	
	//login chauffeur
	  public Optional<Chauffeur> login(String name) {
	        return chauffeurrepository.findChauffeurByMail(name);
	    }
	  
	  //find chauffeur by id
	  public Optional<Chauffeur> getchauffeurid(long id){
		  return chauffeurrepository.findChauffeurByIduser(id);
	  }

	
}
