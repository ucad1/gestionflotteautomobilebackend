package com.sfauto.senflotteautomobiles.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.sfauto.senflotteautomobiles.Model.Vehicule;
import com.sfauto.senflotteautomobiles.dao.AdministrateurRepository;
import com.sfauto.senflotteautomobiles.dao.VehiculeRepository;

@Service
@Transactional
public class VehiculeService {

	private VehiculeRepository vehiculerepository;
	
	private AdministrateurRepository adminrepository;

	@Autowired
	public VehiculeService(VehiculeRepository vehiculerepository,AdministrateurRepository adminrepository) {
	
		this.vehiculerepository = vehiculerepository;
		this.adminrepository = adminrepository;
	}
	
	//enregistrer une vehicule
	public Vehicule saveVehicule(Vehicule vehicule) {
		
		Vehicule vcl = new Vehicule();
		
		vcl.setMatricule(vehicule.getMatricule());
		vcl.setMarque(vehicule.getMarque());
		vcl.setDesignation(vehicule.getDesignation());
		vcl.setNumcartegrise(vehicule.getNumcartegrise());
		vcl.setNumerosasi(vehicule.getNumerosasi());
		vcl.setPuissance(vehicule.getPuissance());
		vcl.setTarifs(vehicule.getTarifs());
		vcl.setTypecarburant(vehicule.getTypecarburant());
		vcl.setModel(vehicule.getModel());
		vcl.setKilometreparcourus(vehicule.getKilometreparcourus());
		vcl.setAnneemodel(vehicule.getAnneemodel());
		vcl.setDateAchat(vehicule.getDateAchat());
		vcl.setEtatVehicule("Disponible");
		vcl.setNombreplaces(vehicule.getNombreplaces());
		
		
		return vehiculerepository.save(vcl);
	}
	//la liste des vehicules
	public List<Vehicule> getAllVehicule(long id){
		
		if(vehiculerepository.getReferenceById(id) != null )
		{
			return vehiculerepository.findAll();
		}else {
			return null;
		}
		
	}
	
	//nombre vehicule
	public int nombreVehicules() {
		return vehiculerepository.nbreVehicule();
	}
	
	//
	public Optional<Vehicule>  getMatricule(String mat) {
		return vehiculerepository.findByMatricule(mat);
	}
	
	//delete vehicule
	  public ResponseEntity<HttpStatus> deleteVehicule(long id) {
		   
		  vehiculerepository.deleteById(id);
	    
	    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	  }
	
	  public Optional<Vehicule> findvehiculeid(long id){
		  return vehiculerepository.findById(id);
	  }
	
	
}
