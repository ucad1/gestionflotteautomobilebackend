package com.sfauto.senflotteautomobiles.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.sfauto.senflotteautomobiles.Model.Accident;
import com.sfauto.senflotteautomobiles.dao.AccidentRepository;
import com.sfauto.senflotteautomobiles.dao.ChauffeurRepository;

@Service
@Transactional
public class AccidentService {

	private ChauffeurRepository chrepo;
	
	private AccidentRepository accrepo;
	
	@Autowired
	public AccidentService(ChauffeurRepository chrepo,  AccidentRepository accrepo) {
		
		this.chrepo = chrepo;
		this.accrepo = accrepo;
	}
	
	
	public ResponseEntity<Accident> declareaccident(long id, Accident acc){
		Accident ac = chrepo.findChauffeurByIduser(id).map(chauffeur ->{
			acc.setChauffeur(chauffeur);
			return accrepo.save(acc);
		}).orElseThrow();
		
		
		return new ResponseEntity<>(ac, HttpStatus.CREATED);
	}
	
	
	
	
}
