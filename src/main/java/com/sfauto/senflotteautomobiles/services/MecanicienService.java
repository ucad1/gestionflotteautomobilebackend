package com.sfauto.senflotteautomobiles.services;

import java.util.*;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfauto.senflotteautomobiles.Model.Mecanicien;
import com.sfauto.senflotteautomobiles.dao.MecanicienRepository;

@Service
@Transactional
public class MecanicienService {
	
	private MecanicienRepository mrepository;

	@Autowired
	public MecanicienService(MecanicienRepository mrepository) {
		
		this.mrepository = mrepository;
	}
	
	
	//all mecanos
	public List<Mecanicien> getAllMecano(){
		
		return mrepository.findAll();
		
	}
	
	//add mecano
	public long addMecano(Mecanicien mecanicien) {
		
		Mecanicien m = new Mecanicien();
		
		m.setNom(mecanicien.getNom());
		m.setPrenom(mecanicien.getPrenom());
		m.setAddress(mecanicien.getAddress());
		m.setMail(mecanicien.getMail());
		m.setNumtel(mecanicien.getNumtel());
		m.setPassword(mecanicien.getPassword());
		m.setPhoto(mecanicien.getPhoto());
		
		m.setRole("MECANICIEN");
		
		return mrepository.save(m).getIduser();
		
		
	}
	
	//login
	public Optional<Mecanicien> login(String mail){
		
		return mrepository.findMecanicienByMail(mail);
		
		
	}
	
	//
	public Optional<Mecanicien> findMecanoid(long id){
		return mrepository.findMecanicienByIduser(id);
	}
	
	public Mecanicien getMecano(long id){
		return mrepository.findMecanicien(id);
	}
	
	

}
