package com.sfauto.senflotteautomobiles.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.sfauto.senflotteautomobiles.Model.Marque;
import com.sfauto.senflotteautomobiles.dao.MarqueRepository;

@Service
@Transactional
public class MarqueService {

	
	private MarqueRepository marquerepository;

	@Autowired
	public MarqueService(MarqueRepository marquerepository) {
		
		this.marquerepository = marquerepository;
	}
	
	//enregistrer une marque
	public long saveMarque(Marque marque) {
		
		Marque mrq = new Marque();
		mrq.setNommarque(marque.getNommarque());
		mrq.setCreationDate(marque.getCreationDate());
		mrq.setUpdationDate(marque.getUpdationDate());
		
		return marquerepository.save(mrq).getId();
		
	}
	
	//la liste des marques
	public List<Marque> getAllMarques(){
		return marquerepository.findAll();
	}
	
	
	//marque par id
	public Optional<Marque> findByCode(long id) {
		
        return marquerepository.findById(id);
    }
	
	//modifier marque
	 public void update(long id, Marque marque) {
	        Optional<Marque> mrq = marquerepository.findById(id);
	        if (mrq.isPresent()) {
	           Marque m =mrq.get();
	            m.setNommarque(marque.getNommarque());
	            m.setUpdationDate(marque.getUpdationDate());
	            
	            marquerepository.save(m);
	        }
	    }
	 
	 //delete marque
	 public void delete(long id) {

	        Optional<Marque> mrq = marquerepository.findById(id);
	        mrq.ifPresent(marquerepository::delete);
	    }
	 
	 //total marques
	 public long totalMarque() {
		 return marquerepository.nbreMarque();
	 }
	
	 
	 //
	 public Optional<Marque> getNomMarque(String nom){
		 return  marquerepository.findByNommarque(nom);
	 }
	 
	 
	 
	
	  public ResponseEntity<HttpStatus> deleteMarque(long id) {
	   
		  marquerepository.deleteById(id);
	    
	    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	  }
	
	
	
	
}
