package com.sfauto.senflotteautomobiles.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sfauto.senflotteautomobiles.Model.Mission;
import com.sfauto.senflotteautomobiles.dao.ChauffeurRepository;
import com.sfauto.senflotteautomobiles.dao.MissionRepository;
import com.sfauto.senflotteautomobiles.dao.VehiculeRepository;

@Service
@Transactional
public class MissionService {
	
	private MissionRepository mrepo;
	
	private VehiculeRepository vrepo;
	
	private ChauffeurRepository chrepo;
	
	@Autowired
	public MissionService( MissionRepository mrepo,VehiculeRepository vrepo,ChauffeurRepository chrepo) {
		this.mrepo =mrepo;
		this.vrepo =vrepo;
		this.chrepo =chrepo;
	}
	
	
	//
	public  ResponseEntity<Mission> ajoutmission(long idv, long idch, Mission missions){
		Mission mssion = chrepo.findChauffeurByIduser(idch).map(ch ->{
			missions.setChauffeur(ch);
			
			return mrepo.save(missions);
		}).orElseThrow();
//		Mission m = vrepo.findById(idv).map(v ->{
//			missions.setVehicule(v);
//			return mrepo.save(missions);
//		}).orElseThrow();
		return new ResponseEntity<>(mssion, HttpStatus.CREATED);
	}

}
