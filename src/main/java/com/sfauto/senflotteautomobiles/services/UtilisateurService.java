package com.sfauto.senflotteautomobiles.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfauto.senflotteautomobiles.Model.Utilisateur;
import com.sfauto.senflotteautomobiles.dao.UtilisateurRepository;

@Service
@Transactional
public class UtilisateurService {
	
	
	private UtilisateurRepository utilisateurrepo;
	
	@Autowired
	public UtilisateurService(UtilisateurRepository utilisateurrepo) {
		this.utilisateurrepo = utilisateurrepo;
	}
	
	
	//login
	public Optional<Utilisateur> login(String mail){
		
		return utilisateurrepo.findUtilisateurByMail(mail);
		
		
	}

}
