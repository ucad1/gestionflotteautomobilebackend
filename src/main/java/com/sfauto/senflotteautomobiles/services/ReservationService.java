package com.sfauto.senflotteautomobiles.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sfauto.senflotteautomobiles.Model.Client;
import com.sfauto.senflotteautomobiles.Model.Reservation;
import com.sfauto.senflotteautomobiles.Model.Utilisateur;
import com.sfauto.senflotteautomobiles.Model.Vehicule;
import com.sfauto.senflotteautomobiles.dao.ClientRepository;
import com.sfauto.senflotteautomobiles.dao.ReservationRepository;
import com.sfauto.senflotteautomobiles.dao.UtilisateurRepository;
import com.sfauto.senflotteautomobiles.dao.VehiculeRepository;

import java.util.*;

@Service
@Transactional
public class ReservationService {

	
	private ReservationRepository reservationrepo;
	private VehiculeRepository vrepo;
	private ClientRepository clientrepo;
	
	private UtilisateurRepository utilisateurepo;
	
	@Autowired
	public ReservationService(ReservationRepository reservationrepo,VehiculeRepository vrepo, ClientRepository clientrepo, UtilisateurRepository utilisateurepo) {
		
		this.reservationrepo = reservationrepo;
		this.vrepo = vrepo;
		this.clientrepo = clientrepo;
		 this.utilisateurepo =  utilisateurepo;
	}
	
	public List<Reservation> listreservation(long iduser){
		
		
		
		if(clientrepo.existsById(iduser)) {
			
			List<Reservation> r = reservationrepo.findAll();
			
			return r;
		}else {
			return null;
		}
	}
	
	//methode qui affiche les reservations par client
	public Optional<Reservation> reservationClientId(Long iduser) {
		
		Optional<Utilisateur> cl =  utilisateurepo.findById(iduser);
		
		if(cl.isPresent()) {
			return  reservationrepo.findReservationByClient(iduser);
		}else {
			return null;
		}
		
		
	}
	
	//Reserver
	public ResponseEntity<Reservation> faireReservation(long id,long iduser, Reservation rvehicule){
		Reservation reservation = vrepo.findById(id).map(vehicule -> {
			rvehicule.setVehicule(vehicule);
			
			rvehicule.setClient(clientrepo.findClientByIduser(iduser).get());
			return reservationrepo.save(rvehicule);
		}).orElseThrow();
		

		return new ResponseEntity<>(reservation, HttpStatus.CREATED);
	}
	
}
