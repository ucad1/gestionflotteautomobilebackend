package com.sfauto.senflotteautomobiles.Utils;

import java.util.Date;

import com.sfauto.senflotteautomobiles.Model.Chauffeur;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseMessage {
	private String message;
}
