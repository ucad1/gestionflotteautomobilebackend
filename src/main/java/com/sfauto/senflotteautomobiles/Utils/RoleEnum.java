package com.sfauto.senflotteautomobiles.Utils;


public enum RoleEnum {

	ROLE_CLIENT("ROLE_CLIENT"),
	ROLE_ADMIN("ROLE_ADMIN"),
	ROLE_CHAUFFEUR("ROLE_CHAUFFEUR"),
	ROLE_MECANICIEN("ROLE_MECANICIEN");
	
	private String name;
	
	RoleEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

