package com.sfauto.senflotteautomobiles.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sfauto.senflotteautomobiles.Model.Chauffeur;


import java.util.Optional;

@Repository
public interface ChauffeurRepository extends JpaRepository<Chauffeur, Long> {
	
	
	Optional<Chauffeur> findChauffeurByMail(String mail);
	
	@Query(value="select count(*) from Chauffeur")
	public int nombreChauffeur();
	
	Optional<Chauffeur> findChauffeurByIduser(long id);
	

}
