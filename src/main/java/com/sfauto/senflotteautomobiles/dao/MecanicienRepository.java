package com.sfauto.senflotteautomobiles.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import com.sfauto.senflotteautomobiles.Model.Mecanicien;

@Repository
public interface MecanicienRepository extends JpaRepository<Mecanicien,Long> {
	
	Optional<Mecanicien> findMecanicienByMail(String mail);
	
	@Query(value="select count(*) from Mecanicien")
	public int nombreMecano();
	
	public Optional<Mecanicien> findMecanicienByIduser(long id);
	

	@Query(value = "SELECT m FROM Mecanicien m WHERE m.iduser=iduser", 
			  nativeQuery = true)
	public Mecanicien findMecanicien( @Param("iduser") long iduser);
}
