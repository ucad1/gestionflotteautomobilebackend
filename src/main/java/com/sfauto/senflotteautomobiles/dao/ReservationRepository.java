package com.sfauto.senflotteautomobiles.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sfauto.senflotteautomobiles.Model.Client;
import com.sfauto.senflotteautomobiles.Model.Reservation;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long>{

	Optional<Reservation> findReservationByClient(Long id);
}