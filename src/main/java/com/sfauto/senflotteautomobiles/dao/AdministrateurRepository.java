package com.sfauto.senflotteautomobiles.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.*;

import com.sfauto.senflotteautomobiles.Model.Administrateur;

@Repository
public interface AdministrateurRepository extends JpaRepository<Administrateur, Long> {

	Optional<Administrateur> findAdministrateurByMail(String mail);
	
	Optional<Administrateur> findAdministrateurByIduser(long iduser);
}
