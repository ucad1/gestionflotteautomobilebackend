package com.sfauto.senflotteautomobiles.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sfauto.senflotteautomobiles.Model.Marque;


@Repository
public interface MarqueRepository extends JpaRepository<Marque,Long> {

	
	public Optional<Marque> findByNommarque(String nom);
	
	
	@Query(value="select count(*) from Marque")
	public long nbreMarque();
	

}
