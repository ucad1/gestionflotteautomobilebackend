package com.sfauto.senflotteautomobiles.dao;

import java.util.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sfauto.senflotteautomobiles.Model.Marque;
import com.sfauto.senflotteautomobiles.Model.Vehicule;

@Repository
public interface VehiculeRepository extends JpaRepository<Vehicule , Long>{

	
	public Optional<Vehicule> findByMatricule(String matricule);
	
	
	//public List<Vehicule> findByAdministrateurId(Long id);
	
	
	@Query(value="select count(*) from Vehicule")
	public int nbreVehicule();
}
