package com.sfauto.senflotteautomobiles.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sfauto.senflotteautomobiles.Model.Utilisateur;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {
	
	
	Optional<Utilisateur> findUtilisateurByMail(String mail);

}
