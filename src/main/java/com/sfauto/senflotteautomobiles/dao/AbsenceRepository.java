package com.sfauto.senflotteautomobiles.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sfauto.senflotteautomobiles.Dtos.AbsenceDtos;
import com.sfauto.senflotteautomobiles.Model.Absence;

@Repository
public interface AbsenceRepository extends JpaRepository<Absence, Long>{

	@Query(value = "SELECT * FROM Absence ", 
			  nativeQuery = true)
	public  List<Absence> listabsence();
	
}
