package com.sfauto.senflotteautomobiles.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sfauto.senflotteautomobiles.Model.Client;

import java.util.*;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long>{
	
	@Query(value="select count(*) from Client")
	public int nombreClient();
	
	Optional<Client> findClientByMail(String mail);
	
	Optional<Client> findClientByIduser(long iduser);
	
}
