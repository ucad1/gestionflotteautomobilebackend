package com.sfauto.senflotteautomobiles.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sfauto.senflotteautomobiles.Model.Mission;

@Repository
public interface MissionRepository extends JpaRepository<Mission, Long>{

}
