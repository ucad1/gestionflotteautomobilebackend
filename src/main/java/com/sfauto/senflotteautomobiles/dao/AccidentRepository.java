package com.sfauto.senflotteautomobiles.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sfauto.senflotteautomobiles.Model.Accident;

@Repository
public interface AccidentRepository extends JpaRepository<Accident,Long>{

}
