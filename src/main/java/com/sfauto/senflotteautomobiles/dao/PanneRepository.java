package com.sfauto.senflotteautomobiles.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sfauto.senflotteautomobiles.Model.Pannes;

public interface PanneRepository extends JpaRepository<Pannes, Long>{

}
