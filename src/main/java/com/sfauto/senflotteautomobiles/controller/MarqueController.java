package com.sfauto.senflotteautomobiles.controller;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParseException;
import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.RequestParam;

import com.sfauto.senflotteautomobiles.Model.Marque;
import com.sfauto.senflotteautomobiles.Model.Vehicule;
import com.sfauto.senflotteautomobiles.services.MarqueService;

import com.sfauto.senflotteautomobiles.Utils.ResponseMessage;
import com.sfauto.senflotteautomobiles.dao.MarqueRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/senflotteautomobile")
public class MarqueController {
	
	
	private MarqueService marqueservice;
	
	private MarqueRepository repository;
	
	@Autowired  ServletContext context;
	
	@Autowired
	public MarqueController(MarqueService marqueservice,MarqueRepository repository) {
		
		this.marqueservice = marqueservice;
		this.repository = repository;
	}


	@PostMapping("/addmarque")
	public ResponseEntity<ResponseMessage> saveMarques(@RequestParam("file") MultipartFile file,
			 @RequestParam("marque") String marque) throws JsonMappingException, JsonProcessingException {
		
		Marque arti = new ObjectMapper().readValue(marque, Marque.class);
        boolean isExit = new File(context.getRealPath("/Images/")).exists();
        if (!isExit)
        {
        	new File (context.getRealPath("/Images/")).mkdir();
        	System.out.println("mk dir.............");
        }
        String filename = file.getOriginalFilename();
        String newFileName = FilenameUtils.getBaseName(filename)+"."+FilenameUtils.getExtension(filename);
        File serverFile = new File (context.getRealPath("/Images/"+File.separator+newFileName));
        try {
        	
        	System.out.println("Image");
       	 FileUtils.writeByteArrayToFile(serverFile,file.getBytes());
       	 
        	
        }catch(Exception e) {
        	e.printStackTrace();
        }
		
		arti.setLogo(newFileName);
		Marque art = repository.save(arti);
        if (art != null)
        {
        	return new ResponseEntity<ResponseMessage>(new ResponseMessage (""),HttpStatus.OK);
        }
        else
        {
        	return new ResponseEntity<ResponseMessage>(new ResponseMessage ("Marque not saved"),HttpStatus.BAD_REQUEST);	
        }
	
		
	}
	@GetMapping("/marques")
	public List<Marque> getMarques(){
		
		 System.out.println("Get all Marques...");
         return marqueservice.getAllMarques();
		
	}
	
	//marque by id
	@GetMapping("/marques/{id}")
	 public ResponseEntity<Marque> post(@PathVariable long id) {
	        Optional<Marque> cat = marqueservice.findByCode(id);
	        return cat.map(ResponseEntity::ok)
	                   .orElseGet(() -> ResponseEntity.notFound()
                                                 .build());
	    }
	
	
	//update marques
	@PutMapping("/updatemarques/{id}")
    public void update(@PathVariable long id, @RequestBody Marque marque) {
		Optional<Marque> cat = marqueservice.findByCode(id);
        if (cat.isPresent()) {
        	marqueservice.update(id, marque);
        } else {
        	marqueservice.saveMarque(marque);
        }
    }
	
	
	//delete marques
	@DeleteMapping("/deletemarque/{id}")
    public void delete(@PathVariable long id) {
		marqueservice.delete(id);
    }

	
	//
	@GetMapping("/totalmarques")
	public long totalMarques(){
		
         return marqueservice.totalMarque();
		
	}
	
	//
	@GetMapping("/marquesname/{nom}")
	public Optional<Marque> getMarquename(@PathVariable String nom){
		
		return marqueservice.getNomMarque(nom);
		
	}
	
	
	//get image vehicule
	 @GetMapping("/ImgMarque/{id}")
	 public byte[] getPhoto(@PathVariable("id") Long id) throws Exception{
		 Marque  vehciule   = marqueservice.findByCode(id).get();
		 return Files.readAllBytes(Paths.get(context.getRealPath("/Images/")+vehciule.getLogo()));
	 }
	 
	 //supprimer marque
	 @DeleteMapping("/marquees/{id}")
	  public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("id") long id) {
	    
		 marqueservice.deleteMarque(id);
	    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	  }
}
