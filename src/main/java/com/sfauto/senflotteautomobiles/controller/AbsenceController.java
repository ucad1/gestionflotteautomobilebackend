package com.sfauto.senflotteautomobiles.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sfauto.senflotteautomobiles.Model.Absence;
import com.sfauto.senflotteautomobiles.services.AbsenceService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/senflotteautomobile")
public class AbsenceController {
	
	private AbsenceService absenceservice;
	
	@Autowired
	public AbsenceController(AbsenceService absenceservice) {
		
		this.absenceservice = absenceservice;
	}
	
	@PostMapping("/demandeabsence/{iduser}")
	public ResponseEntity<Absence> saveabsmecano(@PathVariable long iduser,@RequestBody Absence abs){
		
		return absenceservice.absenceMecano(iduser, abs);
	}
	
	@GetMapping("/listabsences")
	public List<Absence> listAbsence(){
		return absenceservice.getAllAbsence();
	}
	
	/**@GetMapping("/listabsencemecano/{iduser}")
	public Optional<Absence> getAbsence(@PathVariable long iduser){
		return absenceservice.getAbsenceMecano(iduser);
	}**/

}
