package com.sfauto.senflotteautomobiles.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sfauto.senflotteautomobiles.Model.Reservation;
import com.sfauto.senflotteautomobiles.services.ReservationService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/senflotteautomobile")
public class ReservationController {

	private ReservationService rservice;
	
	@Autowired
	public ReservationController(ReservationService rservice) {
		this.rservice =rservice;
	}
	
	//
	@GetMapping("/mesreservations/{iduser}")
	public List<Reservation> mesreservation(@PathVariable long iduser){
		return rservice.listreservation(iduser);
	}
	
	//methode qui permet d'afficher les reservations par client
	//au niveau de la classe ReservationControleur
	@GetMapping("/reservationclient/{id}")
	public Optional<Reservation> reservationClient(@PathVariable Long id){
		
		return rservice.reservationClientId(id);
	}
	
	@PostMapping("/reserver/{id}/{iduser}")
	public ResponseEntity<Reservation> reserver(@PathVariable long id ,@PathVariable long iduser,@RequestBody Reservation rvh){
		
		return rservice.faireReservation(id,iduser, rvh);
	}
}
