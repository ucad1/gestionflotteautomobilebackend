package com.sfauto.senflotteautomobiles.controller;

import java.util.*;


import org.springframework.web.bind.annotation.PathVariable;


import org.springframework.http.ResponseEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sfauto.senflotteautomobiles.Model.Chauffeur;
import com.sfauto.senflotteautomobiles.services.ChauffeurService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/senflotteautomobile")
public class ChauffeurController {
	
	private ChauffeurService chauffeurservice;

	@Autowired
	public ChauffeurController(ChauffeurService chauffeurservice) {
		
		this.chauffeurservice = chauffeurservice;
	}
	
	//get all chauffeurs
	@GetMapping("/chauffeurs")
	public List<Chauffeur> getChauffeurs(){
		
		return chauffeurservice.getAllChauffeur();
	}
	
	
	//total chauffeur
	@GetMapping("/nbrechauffeur")
	public int nbreChauffeurs(){
		
		return chauffeurservice.totalChauff();
	}
	
	@PostMapping("/addchauffeur")
	public long saveChauffeur(@RequestBody Chauffeur ch) {
		return chauffeurservice.ajoutchauffeur(ch);
	}
	
	//login
	 @GetMapping("/authch/{mail}")
	 public ResponseEntity<Chauffeur> login(@PathVariable String mail) {
	        Optional<Chauffeur> user = chauffeurservice.login(mail);
	        return user.map(ResponseEntity::ok)
	                   .orElseGet(() -> ResponseEntity.notFound()
                                               .build());
	    }
	 
	 //
	 @GetMapping("/chauff/{id}")
	 public Optional<Chauffeur> getChauffeurIduser(@PathVariable long id){
		 return chauffeurservice.getchauffeurid(id);
	 }
	

}
