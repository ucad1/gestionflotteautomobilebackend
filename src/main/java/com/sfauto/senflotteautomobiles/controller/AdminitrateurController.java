package com.sfauto.senflotteautomobiles.controller;

import java.io.File;
import java.util.*;

import javax.servlet.ServletContext;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sfauto.senflotteautomobiles.Model.Administrateur;
import com.sfauto.senflotteautomobiles.services.AdministrateurService;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.boot.json.JsonParseException;

import com.sfauto.senflotteautomobiles.Utils.ResponseMessage;
import com.sfauto.senflotteautomobiles.dao.AdministrateurRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/senflotteautomobile")
public class AdminitrateurController {
	
	private AdministrateurService adminservice;
	private AdministrateurRepository repository;
	
	@Autowired  ServletContext context;

	@Autowired
	public AdminitrateurController(AdministrateurService adminservice,AdministrateurRepository repository) {
	
		this.adminservice = adminservice;
		this.repository = repository;
	}
	
	//get all admins
	@GetMapping("/admins")
	public List<Administrateur> getAllAdmins(){
		
		return adminservice.getAlladmin();
		
	}
	
	//ajout administrateur
	@PostMapping("/addadmin")
		public long saveAdmin(@RequestBody Administrateur ch) {
			return adminservice.addAdmin(ch);
		}
	
	//login
		 @GetMapping("/authadmin/{mail}")
		 public ResponseEntity<Administrateur> login(@PathVariable String mail) {
		        Optional<Administrateur> user = adminservice.loginadmin(mail);
		        return user.map(ResponseEntity::ok)
		                   .orElseGet(() -> ResponseEntity.notFound()
	                                               .build());
		    }
		 
		 //
		 @GetMapping("/admin/{id}")
		 public Optional<Administrateur> findadminid(@PathVariable long id){
			 return adminservice.findAdminId(id);
		 }
	
		 @PutMapping("/updateadmin/{iduser}")
		 public ResponseEntity<ResponseMessage> updateAdmin(@RequestParam("file") MultipartFile file,
				 @RequestParam("administrateur") String administrateur,@PathVariable long iduser) throws JsonMappingException, JsonProcessingException {
			 
			 Optional<Administrateur> admin = adminservice.findAdminId(iduser);
			 if(admin.isPresent()) {
				 Administrateur arti = new ObjectMapper().readValue(administrateur, Administrateur.class);
				 
				 Administrateur adm = admin.get();
				  
			        boolean isExit = new File(context.getRealPath("/Images/")).exists();
			        if (!isExit)
			        {
			        	new File (context.getRealPath("/Images/")).mkdir();
			        	System.out.println("mk dir.............");
			        }
			        String filename = file.getOriginalFilename();
			        String newFileName = FilenameUtils.getBaseName(filename)+"."+FilenameUtils.getExtension(filename);
			        File serverFile = new File (context.getRealPath("/Images/"+File.separator+newFileName));
			        try
			        {
			        	System.out.println("Image");
			        	 FileUtils.writeByteArrayToFile(serverFile,file.getBytes());
			        	 
			        }catch(Exception e) {
			        	e.printStackTrace();
			        }
			        
			        adm.setNom(arti.getNom());
			        adm.setPrenom(arti.getPrenom());
			        adm.setMail(arti.getMail());
			        adm.setNumtel(arti.getNumtel());
			        adm.setAddress(arti.getAddress());
			        adm.setPassword(arti.getPassword());
			        
			        arti.setPhoto(newFileName);
			        
			        Administrateur administ = repository.save(adm);
				    
			        if (administ != null)
			        {
	        	return new ResponseEntity<ResponseMessage>(new ResponseMessage ("Admin is updated!!!"),HttpStatus.OK);
			        }
			        else
			        {
			        	return new ResponseEntity<ResponseMessage>(new ResponseMessage ("Admin not updated!!!"),HttpStatus.BAD_REQUEST);	
			        }
			       
			        
			 }
			 
			 else {
			      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			    }
		 }
	

}
