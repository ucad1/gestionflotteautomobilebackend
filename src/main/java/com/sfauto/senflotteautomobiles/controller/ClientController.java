package com.sfauto.senflotteautomobiles.controller;

import java.util.*;


import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.PathVariable;


import org.springframework.http.ResponseEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sfauto.senflotteautomobiles.Model.Client;
import com.sfauto.senflotteautomobiles.Model.Vehicule;
import com.sfauto.senflotteautomobiles.services.ClientService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/senflotteautomobile")
public class ClientController {
	
	
	private ClientService clientservice;

	@Autowired
	public ClientController(ClientService clientservice) {
		
		this.clientservice = clientservice;
	}
	
	
	//get all Clients
		@GetMapping("/clients")
		public List<Client> getClients(){
			
			return clientservice.getAllClient();
		}
		
		
		//ajout client
		@PostMapping("/addclient")
		public long saveClient(@RequestBody Client ch) {
			return clientservice.addClient(ch);
		}

		//login
		 @GetMapping("/authclient/{mail}")
		 public ResponseEntity<Client> login(@PathVariable String mail) {
		        Optional<Client> user = clientservice.loginclient(mail);
		        return user.map(ResponseEntity::ok)
		                   .orElseGet(() -> ResponseEntity.notFound()
	                                               .build());
		    }
	
		 @GetMapping("/totalclient")
		 public int totalclient() {
			 return clientservice.totalClient();
		 }
	
		 
		 //
		 @GetMapping("/clientss/{id}")
		 public Optional<Client> getClientid(@PathVariable long id){
			 return clientservice.fiendClientid(id);
		 }

}
