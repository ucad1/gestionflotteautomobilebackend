package com.sfauto.senflotteautomobiles.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sfauto.senflotteautomobiles.Model.Utilisateur;
import com.sfauto.senflotteautomobiles.services.UtilisateurService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/senflotteautomobile")
public class UtilisateurController {

	private UtilisateurService userservice;
	
	@Autowired
	public UtilisateurController (UtilisateurService userservice) {
		
		this.userservice = userservice;
	}
	
	
	//login
	 @GetMapping("/auth/{mail}")
	 public ResponseEntity<Utilisateur> login(@PathVariable String mail) {
	        Optional<Utilisateur> user = userservice.login(mail);
	        System.out.println("yeees");
	        return user.map(ResponseEntity::ok)
	                   .orElseGet(() -> ResponseEntity.notFound()
                                              .build());
	    }
}
