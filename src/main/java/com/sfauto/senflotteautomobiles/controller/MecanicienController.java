package com.sfauto.senflotteautomobiles.controller;

import java.util.*;

import org.springframework.web.bind.annotation.PathVariable;


import org.springframework.http.ResponseEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import com.sfauto.senflotteautomobiles.Model.Mecanicien;
import com.sfauto.senflotteautomobiles.services.MecanicienService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/senflotteautomobile")
public class MecanicienController {
	
	private MecanicienService mservice;

	@Autowired
	public MecanicienController(MecanicienService mservice) {
		
		this.mservice = mservice;
	}
	
	//get all mecaniciens
	@GetMapping("/mecaniciens")
	public List<Mecanicien> getAllMecano(){
		
		System.out.println("Get all mecanos");
		return mservice.getAllMecano();
	}
	
	
	@PostMapping("/addMecano")
	public long ajoutMecanicien(@RequestBody Mecanicien m) {
		
		return mservice.addMecano(m);
	}
			
	
	//login
		 @GetMapping("/authmecano/{mail}")
		 public ResponseEntity<Mecanicien> login(@PathVariable String mail) {
		        Optional<Mecanicien> user = mservice.login(mail);
		        return user.map(ResponseEntity::ok)
		                   .orElseGet(() -> ResponseEntity.notFound()
	                                               .build());
		    }
	
		 
		 //
		 @GetMapping("/mecanoss/{id}")
		 public Optional<Mecanicien> findMecanoid(@PathVariable long id){
			 return mservice.findMecanoid(id);
		 }
		 
	

}
