package com.sfauto.senflotteautomobiles.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.servlet.ServletContext;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParseException;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;


import com.sfauto.senflotteautomobiles.Model.Vehicule;
import com.sfauto.senflotteautomobiles.Utils.ImageUtility;
import com.sfauto.senflotteautomobiles.Utils.ResponseMessage;
import com.sfauto.senflotteautomobiles.dao.VehiculeRepository;
import com.sfauto.senflotteautomobiles.services.AdministrateurService;
import com.sfauto.senflotteautomobiles.services.VehiculeService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/senflotteautomobile")
public class VehiculeController {

	private VehiculeService vehiculeservice;
	
	private VehiculeRepository vrepo;
	
	private VehiculeRepository repository;
	
	private AdministrateurService adminservice;
	
	public static String uploadDirectory = System.getProperty("user.dir") + "/src/main/webapp/imagedata";

	@Autowired  ServletContext context;
	
	
	@Autowired
	public VehiculeController(AdministrateurService adminservice,VehiculeService vehiculeservice,VehiculeRepository repository,VehiculeRepository vrepo) {
		
		this.vehiculeservice = vehiculeservice;
		this.vrepo = vrepo;
		this.repository =repository;
		this.adminservice = adminservice;
	}
	
	//enregistrer vehicule
///
	@PostMapping("/addvehicule/{iduser}")
	public ResponseEntity<ResponseMessage> addVehicule(@RequestParam("file") MultipartFile file,
			 @RequestParam("vehicule") String vehicule,@PathVariable long iduser) throws JsonParseException , JsonMappingException , Exception{
		
		
		System.out.println("Ok .............");
        Vehicule arti = new ObjectMapper().readValue(vehicule, Vehicule.class);
        boolean isExit = new File(context.getRealPath("/Images/")).exists();
        if (!isExit)
        {
        	new File (context.getRealPath("/Images/")).mkdir();
        	System.out.println("mk dir.............");
        }
        String filename = file.getOriginalFilename();
        String newFileName = FilenameUtils.getBaseName(filename)+"."+FilenameUtils.getExtension(filename);
        File serverFile = new File (context.getRealPath("/Images/"+File.separator+newFileName));
        try
        {
        	System.out.println("Image");
        	 FileUtils.writeByteArrayToFile(serverFile,file.getBytes());
        	 
        }catch(Exception e) {
        	e.printStackTrace();
        }

       
       
       // arti.setFileName(newFileName);
        arti.setFilename(newFileName);
        arti.setAdministrateur(adminservice.findAdminId(iduser).get());
        Vehicule art = repository.save(arti);
        if (art != null)
        {
        	return new ResponseEntity<ResponseMessage>(new ResponseMessage (""),HttpStatus.OK);
        }
        else
        {
        	return new ResponseEntity<ResponseMessage>(new ResponseMessage ("Vehicule not saved"),HttpStatus.BAD_REQUEST);	
        }
		
	}
	
	
	
	///
	
	  @PostMapping("/postvehicule")
	    public ResponseEntity<ImageUploadResponse> postvehicule(@RequestParam("vehicule")Vehicule vehicule, @RequestParam("image") MultipartFile file)
	            throws IOException {
		  
		  vrepo.save(Vehicule.builder()
				  .matricule(vehicule.getMatricule())
				  .marque(vehicule.getMarque())
				  .designation(vehicule.getDesignation())
				  .numcartegrise(vehicule.getNumcartegrise())
				  .numerosasi(vehicule.getNumerosasi())
				  .puissance(vehicule.getPuissance())
				  .tarifs(vehicule.getTarifs())
				  .typecarburant(vehicule.getTypecarburant())
				  .model(vehicule.getModel())
				  .kilometreparcourus(vehicule.getKilometreparcourus())
				  .anneemodel(vehicule.getAnneemodel())
				  .dateAchat(vehicule.getDateAchat())
				  .etatVehicule("Disponible")
				  .nombreplaces(vehicule.getNombreplaces())
				  .build()
				  );
		  
		  return  ResponseEntity.status(HttpStatus.OK)
	                .body(new ImageUploadResponse("Vehicule uploaded successfully: " +
	                        file.getOriginalFilename()));
		  

	  }
	
	//get all vehicles
	@GetMapping("/vehicules/{iduser}")
	public List<Vehicule> getVehicules(@PathVariable long iduser){
		
		return vehiculeservice.getAllVehicule(iduser);
	}
	
	//get image vehicule
	 @GetMapping("/Imgvehicule/{id}")
	 public byte[] getPhoto(@PathVariable("id") Long id) throws Exception{
		 Vehicule vehciule   = repository.findById(id).get() ;
		 return Files.readAllBytes(Paths.get(context.getRealPath("/Images/")+vehciule.getFilename()));
	 }
	
	
	//get all vehicles
		@GetMapping("/totalvehicules")
		public int totalVehicules(){
			
			return vehiculeservice.nombreVehicules();
		}
		
		//get vehicule by matricule
		@GetMapping("/vehicules/matricule/{mat}")
		public Optional<Vehicule> getMatriculevehicule(@PathVariable String mat) {
			
			
			return vehiculeservice.getMatricule(mat);
			
		}
		
		//delete vehicule
		 @DeleteMapping("/vehiculees/{id}")
		  public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("id") long id) {
		    
			 vehiculeservice.deleteVehicule(id);
		    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		  }
	
		 @GetMapping("/vehiculeesid/{id}")
		 public Optional<Vehicule> getvehiculeid(@PathVariable("id") long id){
			return  vehiculeservice.findvehiculeid(id);
		 }
	
	
	
	
}
