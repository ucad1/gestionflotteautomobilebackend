package com.sfauto.senflotteautomobiles.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sfauto.senflotteautomobiles.Model.Accident;
import com.sfauto.senflotteautomobiles.services.AccidentService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/senflotteautomobile")
public class AccidentController {

	private AccidentService accidentservice;
	
	@Autowired
	public AccidentController(AccidentService accidentservice) {
		this.accidentservice = accidentservice;
	}
	
	@PostMapping("/addacident/{iduser}")
	public ResponseEntity<Accident> postaccident(@PathVariable long iduser,@RequestBody Accident accident){
		
		return accidentservice.declareaccident(iduser,accident);
	}
}
