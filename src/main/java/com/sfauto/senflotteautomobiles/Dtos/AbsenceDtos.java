package com.sfauto.senflotteautomobiles.Dtos;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sfauto.senflotteautomobiles.Model.Absence;
import com.sfauto.senflotteautomobiles.Model.Mecanicien;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AbsenceDtos {
	
	private long id;
	private Date dateDebut;
	private Date dateFin;
	private String motifs;
	private long iduser;

}
