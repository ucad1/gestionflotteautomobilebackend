package com.sfauto.senflotteautomobiles.exceptions;

import java.util.Date;

import com.sfauto.senflotteautomobiles.Model.Chauffeur;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResourceNotFoundException {

	private String message;
}
