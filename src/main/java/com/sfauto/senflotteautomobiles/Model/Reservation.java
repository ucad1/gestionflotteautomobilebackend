package com.sfauto.senflotteautomobiles.Model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "reservation")

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Reservation implements Serializable {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private Date dateDebut;
	
	private Date dateFin;
	
	private String commentaire;
	
	@Column(name="etatreservation")
	private String etatreservation;
	
	

	
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	// @JoinColumn(name = "clientid", nullable = false)
	  @OnDelete(action = OnDeleteAction.CASCADE)
	@JsonBackReference(value="client-reserve")  
	//@JsonIgnore
	private Client client;



	  @ManyToOne (fetch = FetchType.LAZY,optional = false)
	  @JsonBackReference(value="vehicule-reserve")
	 // @JoinColumn(name = "vehiculeid", nullable = false)
	  @OnDelete(action = OnDeleteAction.CASCADE)
	 // @JsonIgnore
	private Vehicule vehicule;
}
