package com.sfauto.senflotteautomobiles.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "client",uniqueConstraints = { 
		@UniqueConstraint(columnNames = "numtel") ,
		
	})

@PrimaryKeyJoinColumn( name = "iduser" )

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Client extends Utilisateur  implements Serializable {
	
	@Column(name="numtel")
	private String numtel;

	@JsonManagedReference(value="client-reserve") 
//	 //@JsonIgnore
	  @OneToMany(mappedBy = "client")
	  private List<Reservation> lreservation = new ArrayList<>();
	
	
}
