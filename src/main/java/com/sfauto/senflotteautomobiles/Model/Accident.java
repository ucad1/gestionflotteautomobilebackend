package com.sfauto.senflotteautomobiles.Model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "accident")

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Accident  implements Serializable {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private String matricule;
	
	 @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="GMT")
	 private Date dateaccident;
	
	private String description;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	// @JoinColumn(name = "chauffeurid", nullable = false)
	@JsonBackReference(value="ch-accident")  
	@OnDelete(action = OnDeleteAction.CASCADE)
	//  @JsonIgnore
	private Chauffeur chauffeur;

}
