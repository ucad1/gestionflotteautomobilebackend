package com.sfauto.senflotteautomobiles.Model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "vehicule",
uniqueConstraints = { 
		@UniqueConstraint(columnNames = "matricule") ,
		@UniqueConstraint(columnNames = "numerosasi") ,
		@UniqueConstraint(columnNames = "numcartegrise") ,
		
	})

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Vehicule  implements Serializable {

	@Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private long id;
	
	@Column(name="matricule")
	private String matricule;
	
	@Column(name="marque")
	private String marque;
	
	@Column(name="numerosasi")
	private String numerosasi;
	
	@Column(name="designation")
	private String designation;
	
	@Column(name="numcartegrise")
	private String numcartegrise;
	
	@Column(name="typecarburant")
	private String typecarburant;
	
	@Column(name="puissance")
	private long  puissance;
	
	@Column(name="nombreplaces")
	private int nombreplaces;
	
	@Column(name="kilometreparcourus")
	private BigDecimal kilometreparcourus;
	
	@Column(name="model")
	private String model;
	
	@Column(name="anneemodel")
	private Integer anneemodel;
	
	@Column(name="tarifs")
	private double tarifs;
	
	@Column(name="dateAchat")
	private Date dateAchat;
	
	@Column(name="etatVehicule")
	private String etatVehicule;
	
	@Column(name="filename")
	private String filename;
	

	
	
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	  @JsonBackReference(value="vehicule-admin")
	 // @JoinColumn(name = "adminid", nullable = false)
	  @OnDelete(action = OnDeleteAction.CASCADE)
	 // @JsonIgnore
	private Administrateur administrateur;
	
	@JsonManagedReference(value="vehicule-pannes")
	 //@JsonIgnore
	  @OneToMany(mappedBy = "vehicule")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<Pannes> lpannes = new ArrayList<>();
	
	@JsonManagedReference(value="vehicule-reserve")
	  //@JsonIgnore
	  @OneToMany(mappedBy = "vehicule")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<Reservation> reservations = new ArrayList<>();
	
	
	@JsonManagedReference(value="vehicule-mission") 
	  //@JsonIgnore
     @OneToMany(mappedBy = "vehicule")
	//@LazyCollection(LazyCollectionOption.FALSE)
	private List<Mission> missions = new ArrayList<>();
	
	
	
}
