package com.sfauto.senflotteautomobiles.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Entity
@Table(name = "chauffeur",uniqueConstraints = { 
		@UniqueConstraint(columnNames = "numtel") ,
		
	})

@PrimaryKeyJoinColumn( name = "iduser" )

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Chauffeur extends Utilisateur  implements Serializable  {
	
	
	
	
	@Column(name="numeropermis")
	private String numeropermis;
	
	@Column(name="typepermis")
	private String typepermis;
	
	@Column(name="datedelivrance")
	private Date datedelivrance;
	
	@Column(name="dateexpiration")
	private Date dateexpiration;
	

	
	
	@Column(name="numtel")
	private String numtel;
	
	@Column(name="photo")
	private String photo;
	
	@Column(name="status")
	private String status;
	
	@JsonManagedReference(value="ch-accident")
	  //@JsonIgnore
	  @OneToMany(mappedBy = "chauffeur")
	private List<Accident> accidents = new ArrayList<>();
	
	@JsonManagedReference(value="ch-missions")
	  //@JsonIgnore
	  @OneToMany(mappedBy = "chauffeur")
	private List<Mission> missions = new ArrayList<>();

	@JsonManagedReference(value="ch-pannes")
	  //@JsonIgnore
	  @OneToMany(mappedBy = "chauffeur")
	private List<Pannes> pannes = new ArrayList<>();

	
}

