package com.sfauto.senflotteautomobiles.Model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "absence")

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Absence  implements Serializable {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="numpermis", nullable= true)
	private String numpermis;
	
	
	 @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="GMT")
	private Date dateDebut;
	
	 @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="GMT")
	 private Date dateFin;
	
	private String motifs;
	
	
	
	 @ManyToOne (fetch = FetchType.LAZY,  optional = false)
	  @JsonBackReference
	  //@JoinColumn(name = "mecanicienid", nullable = false)
	  @OnDelete(action = OnDeleteAction.CASCADE)
	  //@JsonIgnore
	private Mecanicien mecanicien;

}
