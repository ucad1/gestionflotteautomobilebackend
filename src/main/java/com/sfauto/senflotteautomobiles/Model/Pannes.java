package com.sfauto.senflotteautomobiles.Model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "pannes")

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Pannes  implements Serializable {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private String matricule;
	
	private String description;
	
	private Date datepanne;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	  //@JoinColumn(name = "chauffeurid", nullable = false)
	  @OnDelete(action = OnDeleteAction.CASCADE)
	  //@JsonIgnore
	@JsonBackReference(value="ch-pannes")
	private Chauffeur chauffeur;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	 // @JoinColumn(name = "vehiculeid", nullable = false)
	  @OnDelete(action = OnDeleteAction.CASCADE)
	  //@JsonIgnore
	@JsonBackReference(value="vehicule-pannes")
	private Vehicule vehicule;
	
	

}
