package com.sfauto.senflotteautomobiles.Model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity 

@Table( name = "utilisateur" )
@Inheritance( strategy = InheritanceType.JOINED )
//@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class Utilisateur {
	
	 @Id
	    @GeneratedValue(strategy=GenerationType.IDENTITY)
	    private Long iduser;
	 
	 @Column(name="nom")
		private String nom;
		
		@Column(name="prenom")
		private String prenom;
		
		@Column(name="mail")
		private String mail;
		
		@Column(name="password")
		private String password;
		
		@Column(name="address")
		private String address;
		
		
		@Column(name="photo")
		private String photo;
	  
	 private String role;
	 

}
