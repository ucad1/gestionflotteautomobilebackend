package com.sfauto.senflotteautomobiles.Model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "marque",
uniqueConstraints = { 
		@UniqueConstraint(columnNames = "nommarque") 
	})

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Marque implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private long id;
	

	@Column(name="nommarque")
	private String nommarque;
	
	@Column(name="creationDate", nullable=false,updatable=false)
	@CreationTimestamp
	 @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="GMT")
	 private Date creationDate;
	
	@Column(name="updationDate", nullable=false,updatable=true)
	@LastModifiedDate
	 @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="GMT")
	private Date updationDate;
	
	@Column(name="logo")
	private String logo;
	
	
	
	

}
